import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Message } from 'semantic-ui-react';
import Validator from 'validator';
import InlineError from '../messages/InlineError';

class ForgotPasswordForm extends  Component {
  state= {
    email: '',
    loading:false,
    errors: {}
  };

  onChange = e =>
    this.setState({
      email:  e.target.value
    });

  onSubmit =()=>{
    const errors = this.validate(this.state.email);

    this.setState({ errors });
    if(Object.keys(errors).length === 0){
      this.setState({ loading: true });
      this.props
        .submit(this.state.email)
        .catch(err => this.setState({
          errors: err.response.data.errors,
          loading: false
        }));
    }
  };

  validate = (email) =>{
    const errors ={};
    if(email && !Validator.isEmail(email))
      errors.email = 'Invalid Email';
    if(!email)
      errors.email = 'Can\'t be blank';
    return errors;
  };

  render(){
    const { email, errors, loading } = this.state;

    return(
      <Form loading={ loading }>
        { errors && errors.global && <Message negative>
          <Message.Header>Something went wrong</Message.Header>
          <p>{ errors && errors.global }</p>
        </Message> }
        <Form.Field error={ errors && !!errors.email }>
          <label htmlFor='email'>Email</label>
          <input
            type='email'
            id='email'
            name='email'
            placeholder='example@example.com'
            value={ email}
            onChange={this.onChange}
          />
          {errors && errors.email && <InlineError text={errors && errors.email}/>}
        </Form.Field>
        <Button
          primary
          onClick={this.onSubmit}
        >Send</Button>
      </Form>
    )
  }
}

ForgotPasswordForm.propTypes = {
  submit: PropTypes.func.isRequired,
};

export default ForgotPasswordForm;